---
layout: post
title:  "Road To Nowhere"
#date:   2019-09-17
excerpt: "A tale about one mans perception about his self-worth."
feature: /assets/img/roadtonowhere.jpg
project: true
tag:
- road to nowhere
- small game
- mental health
- adventure game
- alec holowka
- powerquest
- unity
comments: false
---
<b>Road To Nowhere</b> is a short adventure game about a troubled man named Cohle Jacob.<br><br>
He sets off on an introspective road trip through the United States as he tries to deal with the anxiety of his personal issues and demons. No matter where Cohle runs the anxiety and self-doubt always lingers and brings him to his knees.<br><br>
Will Cohle will finally confront his demons head-on? Or will he be overcome by anxiety and helplessness that leaves him a broken man?<br><br>
The game will be available late 2019 through <b>Itch</b>, <b>Gamejolt</b> and <b>Steam</b> and will be released for free.<br><br>
<b>Road To Nowhere</b> will be available on the following platforms.

    * Windows
    * MacOS
    * Linux

<figure class="full">
        <a href="/assets/img/rtn-ss1.jpg"><img src="/assets/img/rtn-ss1.jpg"></a>
</figure>
<p style align="center">This game is developed using Unity and PowerQuest.<br></p>
<figure class="full">
        <a href="/assets/img/rtn-ss2.jpg"><img src="/assets/img/rtn-ss2.jpg"></a>
</figure>

## Credits:

<b>Cinematopgrahy, Visual FX, Coding and Directed By:</b>

     Dean Thomson

<b>Story and Dialogue Written By:</b>

    Dean Thomson
    David J. Ross

<b>Engine Framework By:</b>

    Dave Lloyd

<b>Music By:</b>

    Scott Buckley
    Daniel Birch

As stated above this game will be <b>available late 2019</b>.
