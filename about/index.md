---
layout: page
title: About 15BiT
tags: [about, 15bit, indie, gamedev, melbourne, australia, unity, retro]
date: 2019-08-14
comments: false
---
![!logo](logo.png)
<br /><br /><br />
**15BiT** is an indie development studio based in **Melbourne, Australia**. Though primarily managed and operated by **Dean Thomson** - heavy metal drummer, and bon vivant - he is also aided at times by author **David J. Ross** and various <i>machine elves</i> he has rescued from <i>The In-between</i>.

They produce old school point-and-click adventure games and other forms of interactive fiction.

For the technical minded our games are creating using the Unity game engine as the main development tool.

On top of Unity we are using a development framework called <i>PowerQuest</i> developed by Dave Lloyd of Powerhoof fame that is specifically designed for point-n-click adventure games.
<br />

<figure class="half">
    <a href="https://www.unity.com"><img src="unity.png"></a>
    <a href="https://www.powerhoof.com"><img src="powerquest.png"></a>
</figure>
<br />
If you have any queries regarding our games, development, personal death-threats don't hesitate to email us at:
{% highlight css %}
support (at) 15bit.site
{% endhighlight %}
